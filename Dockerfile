FROM ruby:2.6.0

RUN apt-get update -qq && apt-get install -y build-essential libpq-dev nodejs
RUN mkdir /web_app
WORKDIR /web_app
COPY Gemfile /web_app/Gemfile
COPY Gemfile.lock /web_app/Gemfile.lock
RUN bundle install
COPY . /web_app
