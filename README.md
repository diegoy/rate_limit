# README

For this test I created a dummy controller for test purposes.
https://bitbucket.org/diegoy/rate_limit/src/master/app/controllers/hello_worlds_controller.rb

And most of the logic is implemented inside theses two classes
https://bitbucket.org/diegoy/rate_limit/src/master/app/services/

I thought about using a PostgreSQL for this test but I didn't want to increase 
the setup complexity.

The code still has an issue with race conditions, the count won't be updated 
correctly if two requests occur in the same time. At the same time a lock in 
this code may increase the run time.


