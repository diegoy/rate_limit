class ApplicationController < ActionController::Base
  def ensure_rate_limit
    unless RateLimitService.permit(request.ip)
      render file: 'errors/error_429', status: 429
    end
  end
end
