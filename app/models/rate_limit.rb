class RateLimit < ApplicationRecord

  def reset
    self.count = 0
  end

  def increment
    self.count += 1
  end
end
