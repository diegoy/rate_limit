class HourlyCallsCount
  def call(identification)
    reset_count_older_than_one_hour(identification)
    rate_limit = RateLimit.find_or_initialize_by(identification: identification,
                                                 minute: Time.current.min)

    rate_limit.increment
    rate_limit.save

    calls_in_the_last_hour(identification)
  end


  def calls_in_the_last_hour(identification)
    RateLimit.where(identification: identification).pluck(:count).reduce(:+)
  end

  def reset_count_older_than_one_hour(identification)
    RateLimit.where(identification: identification)
             .where('updated_at < ?', Time.current - 1.hour)
             .update(count: 0)
  end
end
