
class RateLimitService
  def self.permit(identification)
    quantity = HourlyCallsCount.new.call(identification)
    quantity <= threshold
  end

  def self.threshold
    Rails.configuration.rate_limit_threshold
  end
end
