class CreateRateLimits < ActiveRecord::Migration[5.2]
  def change
    create_table :rate_limits do |t|
      t.string :identification, index: true, unique: true
      t.integer :hour, null: false
      t.integer :count, default: 0
      t.timestamps
    end
  end
end
