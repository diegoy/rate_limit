class ChangeRateLimitsHourToMinute < ActiveRecord::Migration[5.2]
  def change
    rename_column :rate_limits, :hour, :minute
  end
end
