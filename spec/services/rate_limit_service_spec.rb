require 'rails_helper'

describe RateLimitService do
  subject { RateLimitService }

  let(:identification) { "192.168.1.1" }

  context 'without previous calls' do

    it do
      expect(subject.permit(identification)).to eq(true)
    end
  end

  context 'with 99 previous calls' do
    before do
      99.times.each { |_| subject.permit(identification) }
  end

    it do
      expect(subject.permit(identification)).to eq(true)
    end
  end

  context 'with 100 previous calls' do
    before do
      100.times.each { |_| subject.permit(identification) }
    end

    it do
      expect(subject.permit(identification)).to eq(false)
    end
  end

  context 'with 100 calls more than 1 hour ago' do
    before do
      Timecop.travel(Date.current - 61.minutes) do
        100.times.each { |_| subject.permit(identification) }
      end
    end

    it do
      expect(subject.permit(identification)).to eq(true)
    end
  end

  context 'with 100 calls 59 minutes ago' do
    before do
      Timecop.travel(2018, 1, 3, 17, 30, 0) do
        100.times.each { |_| subject.permit(identification) }
      end
    end

    it do
      Timecop.travel(2018, 1, 3, 18, 29, 0) do
        expect(subject.permit(identification)).to eq(false)
      end
    end
  end

  context 'with rate limit threshold as 50' do
    before do
      allow(subject).to receive(:threshold).and_return(50)
    end

    context 'with 50 previous calls' do
      before do
        50.times.each { |_| subject.permit(identification) }
      end

      it do
        expect(subject.permit(identification)).to eq(false)
      end

      context 'with another identification' do
        let(:other_identification) { "192.168.2.3" }

        it do
          expect(subject.permit(other_identification)).to eq(true)
        end
      end
    end

    context 'with 49 previous calls' do
      before do
        49.times.each { |_| subject.permit(identification) }
      end

      it do
        expect(subject.permit(identification)).to eq(true)
      end
    end

    context 'without previous calls' do
      it do
        expect(subject.permit(identification)).to eq(true)
      end
    end

    context 'with 50 previous calls more than 1 hour ago' do
      before do
        Timecop.travel(Date.current - 61.minutes) do
          50.times.each { |_| subject.permit(identification) }
        end
      end

      it do
        expect(subject.permit(identification)).to eq(true)
      end
    end
  end
end
